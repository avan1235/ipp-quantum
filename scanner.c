#include "scanner.h"

static inline bool isCharFromCharsetRange(int character)
{
    return (character >= MIN_CHAR_VALUE && character <= MAX_CHAR_VALUE);
}

static inline bool isEmptyLine(int lastChar, ull_t inputLen)
{
    return (lastChar == NEW_LINE_SYMBOL && inputLen == 0);
}

static inline bool isFullLineCommand(int lastChar, ull_t inputLen)
{
    return (lastChar == NEW_LINE_SYMBOL || inputLen == 0);
}

/**
 * getInputLine read from standard input until the NEW_LINE_SYMBOL or EOF
 * symbol and replaces all the out of range symbols to JUNK_SYMBOL
 * which can be defined in @ref scanner.h file to special use dependent on the
 * usage of the function (which characters are really important)
 * the output does not include the NEW_LINE_SYMBOL
 * @param   linePtr - a pointer where the input line is saved
 * @return  length of the input string saved to line
 *          if error when reading the input then the length is -1
 */
ull_t getInputLine(char **linePtr)
{
    int tempInput;
    char tempChar;
    char *tempCharPtr = NULL;

    ull_t i = 0;
    ull_t curLineSize = INIT_LINE_LEN;

    tempCharPtr = (char *) malloc(curLineSize * sizeof(char));

    if (tempCharPtr == NULL) {
        free(*linePtr);
        return 0;
    }
    else {
        (*linePtr) = tempCharPtr;
    }

    while ((tempInput = getchar()) != EOF && tempInput != NEW_LINE_SYMBOL) {

        if (isCharFromCharsetRange(tempInput)) {
            tempChar = (char) tempInput;
        }
        else {
            tempChar = JUNK_SYMBOL;
        }

        (*linePtr)[i] = tempChar;
        ++i;

        if (i > curLineSize - 1) {
            curLineSize *= 2;
            tempCharPtr = (char *) realloc(*linePtr, curLineSize * sizeof(char));

            if (tempCharPtr == NULL) {
                free(*linePtr);
                return 0;
            }
            else {
                (*linePtr) = tempCharPtr;
            }
        }
    }

    // SPECIAL CASES FOR INPUT CHARS

    // empty line with only NEW_LINE_SYMBOL gets the NEW_LINE_SYMBOL
    // in string to be parsed as the empty input and finish with error
    // (not to be parsed as the last of the all inputs given in input source)
    // the goal is to catch the difference between EOF and NEW_LINE_SYMBOL
    if (isEmptyLine(tempInput, i)) {
        (*linePtr)[i] = NEW_LINE_SYMBOL;
        ++i;
    }

    // when the read input does not ends with NEW_LINE_SYMBOL so it is
    // not a fully correct line of command then it has to be changed
    // into something what will be treated as a junk by the parser
    // so we convert its last char (which we know that exists)
    // into the JUNK_SYMBOL to force parser to treat this line
    // as unusable for interpreting
    if (!isFullLineCommand(tempInput, i)) {
        (*linePtr)[i-1] = JUNK_SYMBOL;
    }

    (*linePtr)[i] = END_OF_STRING;

    return i;
}