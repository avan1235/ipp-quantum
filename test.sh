#!/bin/bash

SEPARATOR="---------------------------------------------------------------------------------------"

if [[ $# -lt 2 ]]
then
    echo "Usage: ./tests.sh <application> <tests-directory> \
    (optionally \"valgrind\" as last parameter for memory tests)"
    exit 1
fi

if [[ ! -f "$1" ]]
then
    echo -e "File $1 does not exist\nLeaving testing script..."
    exit 1
fi

if [[ ! -d "$2" ]]
then
    echo -e "Directory $2 does not exist\nLeaving testing script..."
    exit 1
fi


error=0
valError=0
testsNum=0

SAVE_IFS=$IFS
IFS=$(echo -en "\n")

for f in $2/*.in
do
    # delete double slashes from file path
    f=$(echo "$f" | sed s#//*#/#g)

    $1 <${f} >${f%in}tempout 2>${f%in}temperr
   
    echo "File ${f%in}out test:"
    if diff ${f%in}tempout ${f%in}out >/dev/null 2>&1
    then
	    echo -e "\e[92mGenerated standard output file is correct\e[39m"
    else
	    echo -e "\e[91m---> There is a difference in generated standard output file <---\e[39m"
	    error=$((error+1))
    fi

    echo "File ${f%in}err test:"
    if diff ${f%in}temperr ${f%in}err >/dev/null 2>&1
    then
	    echo -e "\e[92mGenerated error output file is correct\e[39m"
    else
	    echo -e "\e[91m---> There is a difference in generated error output file <---\e[39m"
	    error=$((error+1))
    fi

    # remove temporary test files from tests directory
    rm -f ${f%in}tempout ${f%in}temperr

    # test for memory leaks when the special argument 'valgrind' added to command
    # for bigger data main-stacksize parameter of valgrind should be increased
    # and the limit of system stack size also should be increased
    if [[ "$3" = "valgrind" ]]
    then
        echo ${SEPARATOR}
        valgrind --error-exitcode=15 --leak-check=full --show-leak-kinds=all \
                 --main-stacksize=134217728 --errors-for-leak-kinds=all $1 \
                 <$f >/dev/null 2>&1
        if [[ $? -eq 15 ]]
        then
            echo -e "\e[91m---> Memory error reported by Valgrind for data in input file $f <---\e[39m"
            echo -e "\e[91mConsider using the command: valgrind --error-exitcode=15 \
                     --leak-check=full --show-leak-kinds=all --main-stacksize=134217728 \
                     --errors-for-leak-kinds=all $1 <$f\e[39m"
            valError=$((valError+1))
        else
            echo -e "\e[92mNo memory errors\e[39m reported by Valgrind for data in input file $f"
        fi
    fi

    # count done tests for stats
    testsNum=$((testsNum+2))

    echo ${SEPARATOR}
done

IFS=${SAVE_IFS}

# print the stats from tests
echo -e "\e[1m\nTESTS RESULTS:\e[0m"
echo -e "\t\e[92m$((testsNum-error)) correctly\e[39m passed tests of input/output files tests"
echo -e "\t\e[91m$((error)) errors\e[39m found in input/output files tests"

if [[ "$3" = "valgrind" ]]
then
    echo -e "\tValgrind reported $((valError)) errors during tests"
fi