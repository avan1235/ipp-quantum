#include "energy_set.h"

/**
 * When there was an error allocating the new energy, function
 * return the MEMORY_ALLOC_ERROR (NULL)
 *
 * @param value     an unsigned long long value which will be kept in energy_t
 * @return          a pointer to the new allocated energy_t
 */
energy_t *initNewEnergy(ull_t value)
{
    energy_t *newEnergyPtr = (energy_t *) malloc(sizeof(energy_t));

    if (newEnergyPtr == NULL) {
        return MEMORY_ALLOC_ERROR;
    }

    newEnergyPtr->energyValue = value;
    newEnergyPtr->parentEnergy = NULL;
    newEnergyPtr->howManyPointsThisEnergy = INIT_NUM_OF_POINTERS;

    return newEnergyPtr;
}

/**
 * @param   childEnergy is the node of energy tree from which we start
 *          looking for a parent energy (one which does not have parent)
 * @return  a pointer to the main parent energy pointed by child energy
 */
energy_t *rootPointedEnergy(energy_t *childEnergy)
{
    while ((*childEnergy).parentEnergy != NULL) {
        childEnergy = (*childEnergy).parentEnergy;
    }

    return childEnergy;
}

void freeEnergiesNotPointedByEnergies(energy_t *energyPtr)
{
    if (energyPtr != NULL) {
        energyPtr->howManyPointsThisEnergy -= 1;

        if (energyPtr->howManyPointsThisEnergy == 0) {
            energy_t *parentEnergyPtr = energyPtr->parentEnergy;
            free(energyPtr);
            freeEnergiesNotPointedByEnergies(parentEnergyPtr);
        }
    }
}