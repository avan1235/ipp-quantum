#ifndef HISTORY_TREE_H
#define HISTORY_TREE_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "input_properties.h"
#include "energy_set.h"

#define NO_ENERGY 0

typedef struct node* tree_t;

tree_t initTree();

bool insertHistory(tree_t *treePtr, const char *histStr);

bool validHistory(tree_t tree, const char *histStr);

void freeAllHistory(tree_t tree);

void freeHistory(tree_t tree, const char *histStr);

bool addEnergyToHistory(tree_t tree, const char *histStr, ull_t energyValue);

ull_t getEnergyOfHistory(tree_t tree, const char *histStr, bool *hasEnergy);

bool makeHistoriesEqual(tree_t tree, const char *histStrFst, const char *histStrSnd);

#endif /* #ifndef HISTORY_TREE_H */

