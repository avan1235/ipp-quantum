#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "input_properties.h"
#include "history_tree.h"

#define TASK_MAX_LEN 7
#define ARG_INIT_LEN 128

#define MAX_ULL_VALUE_STRING "18446744073709551615"

#define DECLARE_COMMAND "DECLARE"
#define REMOVE_COMMAND "REMOVE"
#define VALID_COMMAND "VALID"
#define ENERGY_COMMAND "ENERGY"
#define EQUAL_COMMAND "EQUAL"

#define ERROR_TASK 0
#define DECLARE 1
#define REMOVE 2
#define VALID 3
#define ENERGY 4
#define EQUAL 5

#define COPY_ERROR NULL


ull_t getInputLine(char **linePtr);

void parseInput(char *line, ull_t lineLen, tree_t *treePtr);

#endif //PARSER_H
