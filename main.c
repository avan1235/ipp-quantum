#include "input_properties.h"
#include "scanner.h"
#include "parser.h"
#include "history_tree.h"

int main()
{
    tree_t historyTree = initTree();
    
    if (historyTree != MEMORY_ALLOC_ERROR) {
        char *line = NULL;
        ull_t lineLen;

        while ((lineLen = getInputLine(&line)) > MIN_INPUT_LINE_LEN) {
            parseInput(line, lineLen, &historyTree);
            free(line);
        }

        free(line); // the last input line has to be freed separately
    }
    else { // memory allocation error
        exit(EXIT_FAILURE);
    }

    freeAllHistory(historyTree);
    free(historyTree);

    return EXIT_SUCCESS;
}
