#ifndef TESTER_ENERGY_SET_H
#define TESTER_ENERGY_SET_H

#include <stdio.h>
#include <stdlib.h>

#include "input_properties.h"

#define INIT_NUM_OF_POINTERS 1

/**
 * Type energy_t to represent the tree of equivalence of different
 * histories in history tree. Every node can be the last one and
 * then its pointer parentEnergy is NULL or can have a parent
 * which appeared during EQUAL operation.
 * By EQUAL operation we create a DAG of energies and then can
 * easily remove only energies not pointed by some nodes from
 * history tree (or through other energies also from nodes)
 */
typedef struct energy
{
    ull_t energyValue;
    ull_t howManyPointsThisEnergy;
    struct energy *parentEnergy;
} energy_t;

energy_t *initNewEnergy(ull_t value);

energy_t *rootPointedEnergy(energy_t *childEnergy);

void freeEnergiesNotPointedByEnergies(energy_t *energyPtr);

#endif //TESTER_ENERGY_SET_H
