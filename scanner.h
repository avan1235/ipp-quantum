#ifndef TESTER_SCANNER_H
#define TESTER_SCANNER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "input_properties.h"

#define INIT_LINE_LEN 128

#define MIN_CHAR_VALUE 1
#define MAX_CHAR_VALUE 90

// for writing into input string when get
// the the char out of the range of program
// specified charset defined by
// MIN_CHAR_VALUE and MAX_CHAR_VALUE
// Have to be defined to be in range
// from 0 to 255 to fit into char
#define JUNK_SYMBOL (MAX_CHAR_VALUE + 1)


ull_t getInputLine(char **linePtr);

#endif //TESTER_SCANNER_H
