#include "parser.h"

/**
 * structure command_t can have at most one task and two args
 * all the fields are default set to null when initializing
 * the new command
 */
typedef struct command
{
    char *task;
    char *arg1;
    char *arg2;
} command_t;

command_t initCommand()
{
    command_t newCommand;
    newCommand.task = NULL;
    newCommand.arg1 = NULL;
    newCommand.arg2 = NULL;

    return newCommand;
}

static bool stringEquals(const char *s1, const char *s2)
{
    return (strcmp(s1, s2) == 0);
}

static bool canReadMoreArguments(const char *lastReadPtr, ull_t length)
{
    return (length > 0 && lastReadPtr != NULL && (*lastReadPtr) == SPACE_SYMBOL);
}

static bool canIgnoreInputLine(const char *line)
{
    return (*line == COMMENT_SYMBOL || *line == NEW_LINE_SYMBOL);
}

static bool hasTask(const char *argStart, ull_t lineLen)
{
    return (argStart != NULL && *argStart == SPACE_SYMBOL && lineLen > 0);
}

static bool hasOneArg(const char *argStart, const char *endLine)
{
    return (argStart != NULL && *argStart == END_OF_STRING && endLine == NULL);
}

static bool hasTwoArgs(const char *argStart, const char *endLine, const char *argValue) {
    return (argStart != NULL && *argStart == SPACE_SYMBOL
            && endLine != NULL && *endLine == END_OF_STRING
            && argValue != NULL && *argValue != END_OF_STRING);
}

static bool isCharFromAlphabetRange(char c)
{
    return (c >= FIRST_LETTER && c <= LAST_LETTER);
}

static bool isCharFromNumberRange(char c)
{
    return (c >= FIRST_NUMBER && c <= LAST_NUMBER);
}

static int commandChoice(const char *task)
{
    if(stringEquals(task, DECLARE_COMMAND)) {
        return DECLARE;
    }
    else if (stringEquals(task, REMOVE_COMMAND)) {
        return REMOVE;
    }
    else if (stringEquals(task, VALID_COMMAND)) {
        return VALID;
    }
    else if (stringEquals(task, ENERGY_COMMAND)) {
        return ENERGY;
    }
    else if (stringEquals(task, EQUAL_COMMAND)) {
        return EQUAL;
    }
    else {
        return ERROR_TASK;
    }
}

static void printValidResult(tree_t tree, const char *histStr)
{
    fprintf(stdout, validHistory(tree, histStr) ? "YES\n" : "NO\n");
}

static void printError()
{
    fprintf(stderr, "ERROR\n");
}

static void printOK()
{
    fprintf(stdout, "OK\n");
}

static void printEnergyOfHistory(tree_t tree, const char *histStr)
{
    bool hasEnergy = false;
    ull_t energyToPrint = getEnergyOfHistory(tree, histStr, &hasEnergy);

    if (hasEnergy) {
        fprintf(stdout, "%" PRIu64 "\n", energyToPrint);
    }
    else {
        printError();
    }
}

static bool isHistoryString(const char *history)
{
    size_t i = 0;
    while (history[i] != END_OF_STRING) {
        if (history[i] < MIN_HIST_VAL || history[i] > MAX_HIST_VAL) {
            return false;
        }
        ++i;
    }

    if (i == 0)
        return false;
    else
        return true;
}

static bool isNumberString(const char *number)
{
    size_t i = 0;
    while (number[i] != END_OF_STRING) {
        if (!isCharFromNumberRange(number[i])) {
            return false;
        }
        ++i;
    }

    if (i == 0)
        return false;
    else
        return true;
}

/**
 * checks if given string represents a number
 * from range from 1 to 2^64−1
 */
static bool isValidULLNumberString(const char *number)
{
    if (isNumberString(number)) {
        while (*number == FIRST_NUMBER) {
            ++number;
        }

        if (*number == END_OF_STRING) {
            return false;
        }
        else {
            if (strlen(number) == strlen(MAX_ULL_VALUE_STRING)) {
                // check lexicographically the given string
                return (strcmp(number, MAX_ULL_VALUE_STRING) <= 0);
            }
            else {
                // compare just the lengths of the number strings
                return (strlen(number) < strlen(MAX_ULL_VALUE_STRING));
            }
        }
    }
    else {
        return false;
    }
}

/**
 * converts the validated unsigned long long string representation
 * (which is greater or equal than 1 and smaller than 2^64)
 * into the ull_t variable by analyzing the next chars of string
 */
static ull_t stringToULL(const char *number)
{
    while (*number == FIRST_NUMBER) {
        ++number;
    }

    size_t strLen = strlen(number);
    ull_t resultNum = 0;
    char const *endPtr = number + strLen;

    // tricky operating on bits of given string converts
    // next chars into numerical value
    while (number < endPtr) {
        resultNum = (resultNum << 1) + (resultNum << 3) + *(number++) - FIRST_NUMBER;
    }

    return resultNum;
}

/**
 * @param   copyTo is a pointer to char array where the entry is copied
 * @param   copyFrom is a char array that will be copied
 * @param   copyFromLen is a pointer to initial length of copyFrom which
 *          decreases when reading from copyFrom
 * @return  a pointer to the next part of the command on a FREE_SPACE_SYMBOL
 */
static char* copyTaskAsString(char **copyTo, char *copyFrom, ull_t *copyFromLen)
{
    char *tempCharPtr = NULL;
    size_t i = 0;

    tempCharPtr = (char *) malloc((TASK_MAX_LEN + 1) * sizeof(char));

    if (tempCharPtr == NULL) {
        free(*copyTo);
        return COPY_ERROR;
    }
    else {
        (*copyTo) = tempCharPtr;
    }

    while (i < TASK_MAX_LEN && (*copyFromLen) > 0
           && isCharFromAlphabetRange(copyFrom[i])) {
        (*copyTo)[i] = copyFrom[i];
        ++i;
        --(*copyFromLen);
    }

    (*copyTo)[i] = END_OF_STRING;

    return &(copyFrom[i]);
}

/**
 * works in the same way as @ref copyTaskAsString but
 * allocates memory dynamically
 */
static char* copyArgAsString(char **copyTo, char *copyFrom, ull_t *copyFromLen)
{
    char *tempCharPtr;
    size_t curStringSize = ARG_INIT_LEN;
    size_t i = 0;

    tempCharPtr = (char *) malloc(curStringSize * sizeof(char));

    if (tempCharPtr == NULL) {
        free(*copyTo);
        return COPY_ERROR;
    }
    else {
        (*copyTo) = tempCharPtr;
    }

    while ((*copyFromLen) > 0 && isCharFromNumberRange(copyFrom[i])) {
        (*copyTo)[i] = copyFrom[i];
        ++i;
        --(*copyFromLen);

        if (i > curStringSize - 1) {
            curStringSize *= 2;
            tempCharPtr = (char *) realloc(*copyTo, curStringSize * sizeof(char));

            if (tempCharPtr == NULL) {
                free(*copyTo);
                return COPY_ERROR;
            }
            else {
                (*copyTo) = tempCharPtr;
            }
        }
    }

    (*copyTo)[i] = END_OF_STRING;

    return &(copyFrom[i]);
}

void parseInput(char *line, ull_t lineLen, tree_t *treePtr)
{
    if (!canIgnoreInputLine(line)) {
        command_t command = initCommand();

        char *arg1Start = NULL;
        char *arg2Start = NULL;
        char *endLine = NULL;

        arg1Start = copyTaskAsString(&command.task, line, &lineLen);

        if (arg1Start == COPY_ERROR) { // string memory allocation error
            goto cleanup;
        }

        if (canReadMoreArguments(arg1Start, lineLen)) {
            arg2Start = copyArgAsString(&command.arg1, arg1Start + 1, &lineLen);

            if (arg2Start == COPY_ERROR) { // string memory allocation error
                goto cleanup;
            }
        }

        if (canReadMoreArguments(arg2Start, lineLen)) {
            endLine = copyArgAsString(&command.arg2, arg2Start + 1, &lineLen);

            if (endLine == COPY_ERROR) { // string memory allocation error
                goto cleanup;
            }
        }

        if (hasTask(arg1Start, lineLen)) {
            /**
             * command has EXACTLY one argument and this argument is
             * a valid history string
             */
            if (hasOneArg(arg2Start, endLine) && isHistoryString(command.arg1))
            {
                switch (commandChoice(command.task))
                {
                    case DECLARE: {
                        if (insertHistory(treePtr, command.arg1)) {
                            printOK();
                        } else { // history node memory allocation error
                            goto cleanup;
                        }
                    } break;

                    case REMOVE: {
                        freeHistory(*treePtr, command.arg1);
                        printOK();
                    } break;

                    case VALID: {
                        printValidResult(*treePtr, command.arg1);
                    } break;

                    case ENERGY: {
                        printEnergyOfHistory(*treePtr, command.arg1);
                    } break;

                    default: { // ERROR_TASK included in this case
                        printError();
                    }
                }
            }

            /**
             * command has EXACTLY two arguments and the first of them
             * is a valid history string
             */
            else if (hasTwoArgs(arg2Start, endLine, command.arg2)
                     && isHistoryString(command.arg1))
            {
                switch (commandChoice(command.task))
                {
                    case ENERGY: {
                        if (isValidULLNumberString(command.arg2)
                            && validHistory(*treePtr, command.arg1)) {

                            ull_t numValue = stringToULL(command.arg2);

                            if (!addEnergyToHistory(*treePtr, command.arg1, numValue)) {
                                goto cleanup; // energy memory allocation error
                            }
                            printOK();
                        }
                        else {
                            printError();
                        }
                    } break;

                    case EQUAL: {
                        if (isHistoryString(command.arg2)) {
                            // if there are no given histories
                            // in the tree then print ERROR
                            makeHistoriesEqual(*treePtr, command.arg1, command.arg2) ?
                            printOK() : printError();
                        }
                        else {
                            printError();
                        }
                    } break;

                    default: { // ERROR_TASK included in this case
                        printError();
                    }
                }
            }
            else { // bad arguments
                printError();
            }
        }
        else { // bad command task
            printError();
        }

        free(command.task);
        free(command.arg1);
        free(command.arg2);
        return; // exit parser here when cleanup not needed


        /*
         * on any memory allocation problem in some step of
         * parsing the given input line, free all memory and
         * exit from program with EXIT_FAILURE code
         */
        cleanup:
        {
            freeAllHistory(*treePtr);
            free(*treePtr);
            free(line);
            exit(EXIT_FAILURE);
        }
    }
}