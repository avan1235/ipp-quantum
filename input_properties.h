#ifndef INPUT_PROPERTIES_H
#define INPUT_PROPERTIES_H

#include <inttypes.h>

#define FIRST_LETTER 'A'
#define LAST_LETTER 'Z'
#define FIRST_NUMBER '0'
#define LAST_NUMBER '9'

#define MIN_HIST_VAL '0'
#define MAX_HIST_VAL '3'
#define HIST_ELEMENTS_SIZE (MAX_HIST_VAL - MIN_HIST_VAL + 1)

#define MIN_INPUT_LINE_LEN 0

#define COMMENT_SYMBOL '#'
#define NEW_LINE_SYMBOL '\n'
#define END_OF_STRING '\0'
#define SPACE_SYMBOL ' '

#define MEMORY_ALLOC_ERROR NULL

typedef uint64_t ull_t;

#endif //INPUT_PROPERTIES_H
