#include "history_tree.h"

/**
 * Type node_t is a part of history tree which node can have
 * at most 4 children and one pointer to some energy_t value.
 * If the pointer in array of children[i] is not NULL pointer
 * then the history extended by (char) (i-'0') extends
 * given tree node
 */
struct node
{
    struct node *child[HIST_ELEMENTS_SIZE];
    energy_t *energy;
} node_t;


static bool isOddNumber(ull_t number)
{
    return (number % 2 == 1);
}

static bool stringEquals(const char *s1, const char *s2)
{
    return (strcmp(s1, s2) == 0);
}

/**
 * avgVal calculates the average value of two unsigned long long values
 * taking care about the variable ranges by first dividing them
 * and checking if they are odd numbers or not
 */
static ull_t avgValue(ull_t fstVal, ull_t sndVal)
{
    if (isOddNumber(fstVal) && isOddNumber(sndVal)) {
        return (((fstVal / 2) + (sndVal / 2)) + 1);
    }
    else {
        return ((fstVal / 2) + (sndVal / 2));
    }
}

/**
 * When there was an error allocating the new node, function
 * return the MEMORY_ALLOC_ERROR (NULL)
 *
 * @return      a pointer (type tree_t) to the new allocated node_t
 */
tree_t initTree()
{
    tree_t newTree = (tree_t) malloc(sizeof(node_t));

    if (newTree == NULL) {
        return MEMORY_ALLOC_ERROR;
    }

    for (int i = 0; i < HIST_ELEMENTS_SIZE; ++i) {
        newTree->child[i] = NULL;
    }

    newTree->energy = NULL;

    return newTree;
}

/*
 * Inserts new history to the given history tree
 * When encounters the memory allocation problems
 * then return false to clean the program and exit
 * in other cases return true
 */
bool insertHistory(tree_t *treePtr, const char *histStr)
{
    while (*histStr != END_OF_STRING) {

        tree_t *childTreePtr = &((*treePtr)->child[*histStr - MIN_HIST_VAL]);

        // the history does not exists in tree
        if (*childTreePtr == NULL) {

            *childTreePtr = initTree();

            if (*childTreePtr == MEMORY_ALLOC_ERROR) {
                return false;
            }
        }

        treePtr = childTreePtr;
        ++histStr;
    }

    return true;
}

/**
 * @param   tree is a pointer to node_t in which we look for histStr
 * @param   histStr is the history that we are looking for given as string
 * @return  a pointer to the pointer that points in history tree to the
 *          energy from the end of given history
 */
static energy_t **findHistoryEnergy(tree_t tree, const char *histStr)
{
    while (*histStr != END_OF_STRING) {

        if (tree->child[*histStr - MIN_HIST_VAL] == NULL) {
            return NULL;
        }
        else {
            tree = tree->child[*histStr - MIN_HIST_VAL];
        }

        ++histStr;
    }

    return &(tree->energy);
}

/**
 * @param   tree in which we search for given history
 * @param   histStr is a valid history string
 * @return  true when history exists in tree else return false
 */
bool validHistory(tree_t tree, const char *histStr)
{
    energy_t **energyPtrToPtr = findHistoryEnergy(tree, histStr);

    if (energyPtrToPtr == NULL) {
        return false;
    }
    else {
        return true;
    }
}

/**
 * If the given history exists in tree function adds new energy to this
 * history and returns true. When there is no such history in tree
 * function returns false.
 */
bool addEnergyToHistory(tree_t tree, const char *histStr, ull_t energyValue)
{
    if (validHistory(tree, histStr)) {

        energy_t **energyPtrToPtrToChange =
                findHistoryEnergy(tree, histStr);

        if (*energyPtrToPtrToChange == NULL) {
            energy_t *newEnergy = initNewEnergy(energyValue);

            if (newEnergy != NULL) {
                (*energyPtrToPtrToChange) = newEnergy;
            }
            else {
                return false;
            }
        }
        else {
            energy_t *energyPtrToChange = rootPointedEnergy(*energyPtrToPtrToChange);
            (*energyPtrToChange).energyValue = energyValue;
        }

        return true;
    }
    else {
        return false;
    }
}

/**
 * Points two histories to the same energy node or does nothing if already
 * they are pointed to the same energy.
 * Function returns true when the process of making histories equal could be
 * done and false when any history does not exists in history tree or both
 * histories doesn't have energy assigned to it
 */
bool makeHistoriesEqual(tree_t tree, const char *histStrFst, const char *histStrSnd)
{
    if (validHistory(tree, histStrFst) && validHistory(tree, histStrSnd)) {

        // the special case when the given histories are valid, they have
        // no energy assigned to them but they are the same history
        // so they are always equal to each other
        if (stringEquals(histStrFst, histStrSnd)) {
            return true;
        }

        energy_t **fstEnergyPtrToPtr = findHistoryEnergy(tree, histStrFst);
        energy_t **sndEnergyPtrToPtr = findHistoryEnergy(tree, histStrSnd);

        // the cases when only one history has energy assigned to it
        if (*fstEnergyPtrToPtr == NULL && *sndEnergyPtrToPtr != NULL) {
            *fstEnergyPtrToPtr = rootPointedEnergy(*sndEnergyPtrToPtr);
            rootPointedEnergy(*sndEnergyPtrToPtr)->howManyPointsThisEnergy += 1;
        }
        else if (*fstEnergyPtrToPtr != NULL && *sndEnergyPtrToPtr == NULL) {
            *sndEnergyPtrToPtr = rootPointedEnergy(*fstEnergyPtrToPtr);
            rootPointedEnergy(*fstEnergyPtrToPtr)->howManyPointsThisEnergy += 1;
        }
        // the case when both histories have some energy assigned
        else if (*fstEnergyPtrToPtr != NULL && *sndEnergyPtrToPtr != NULL) {
            energy_t *fstRootEnergyPtr = rootPointedEnergy(*fstEnergyPtrToPtr);
            energy_t *sndRootEnergyPtr = rootPointedEnergy(*sndEnergyPtrToPtr);

            //but they do not point to the same root energy
            if (fstRootEnergyPtr != sndRootEnergyPtr) {
                ull_t fstVal = fstRootEnergyPtr->energyValue;
                ull_t sndVal = sndRootEnergyPtr->energyValue;

                ull_t avgVal = avgValue(fstVal, sndVal);

                // randomize which energy points to the second one
                // not to create a directed structure in one side
                srand(time(NULL));

                if (rand() % 2) {
                    fstRootEnergyPtr->energyValue = avgVal;
                    sndRootEnergyPtr->parentEnergy = fstRootEnergyPtr;
                    fstRootEnergyPtr->howManyPointsThisEnergy += 1;
                }
                else {
                    sndRootEnergyPtr->energyValue = avgVal;
                    fstRootEnergyPtr->parentEnergy = sndRootEnergyPtr;
                    sndRootEnergyPtr->howManyPointsThisEnergy += 1;
                }
            }
        }
        else {
            // if none of the histories has energy assigned to it
            return false;
        }

        // success operation for good input data
        return true;
    }
    else {
        // when the input history does not exist in history tree
        return false;
    }
}

/**
 * Function returns the value of energy and sets the *hasEnergy to true if
 * the given history is valid and has energy assigned to it or returns
 * special value NO_ENERGY which should not be used in future because
 * also the value of *hasEnergy is changed to false
 */
ull_t getEnergyOfHistory(tree_t tree, const char *histStr, bool *hasEnergy)
{
    energy_t **energyPtrToPtr = NULL;

    if (validHistory(tree, histStr)
        && (*(energyPtrToPtr = findHistoryEnergy(tree, histStr))) != NULL) {

        *hasEnergy = true;
        return rootPointedEnergy(*energyPtrToPtr)->energyValue;
    }
    else {
        *hasEnergy = false;
        return NO_ENERGY;
    }
}

static void freeEnergiesFromTree(tree_t tree)
{
    freeEnergiesNotPointedByEnergies(tree->energy);
    tree->energy = NULL;
}

void freeAllHistory(tree_t tree)
{
    if (tree != NULL) {
        for (int i = 0; i < HIST_ELEMENTS_SIZE; ++i) {
            if (tree->child[i] != NULL) {
                freeAllHistory(tree->child[i]);
                free(tree->child[i]);
                tree->child[i] = NULL;
            }
        }
        freeEnergiesFromTree(tree);
    }
}

void freeHistory(tree_t tree, const char *histStr)
{
    char curState = *histStr;
    char nextState = *(histStr + 1);

    if (nextState == END_OF_STRING) {
        freeAllHistory(tree->child[curState - MIN_HIST_VAL]);
        free(tree->child[curState - MIN_HIST_VAL]);
        tree->child[curState - MIN_HIST_VAL] = NULL;
    }
    else if (tree->child[curState - MIN_HIST_VAL] != NULL) {
        ++histStr;
        freeHistory(tree->child[curState - MIN_HIST_VAL], histStr);
    }
}